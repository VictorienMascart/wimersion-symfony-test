<?php

namespace App\Repository;

use App\Entity\Courses;
use App\Entity\Taxis;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Taxis|null find($id, $lockMode = null, $lockVersion = null)
 * @method Taxis|null findOneBy(array $criteria, array $orderBy = null)
 * @method Taxis[]    findAll()
 * @method Taxis[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaxisRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Taxis::class);
    }

    public function findAllTaxisForParam(Courses $course){
        return $this->createQueryBuilder('t')
            ->select('t','ta','se','ve')
            ->join('t.tarif', 'ta')
            ->join('t.secteur', 'se')
            ->join('t.vehicule', 've')
            ->where('ta.tf_prixttc_km <= ?1')
            ->andWhere('se.se_libelle = ?2')
            ->setParameter(1, $course->getTarif()->getTfPrixttcKm())
            ->setParameter(2, $course->getSecteur()->getSeLibelle())
            ->getQuery()
            ->getResult()
            ;
    }

    // /**
    //  * @return Taxis[] Returns an array of Taxis objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Taxis
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
