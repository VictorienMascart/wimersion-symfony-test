<?php

namespace App\Repository;

use App\Entity\Courses;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr\Join;

/**
 * @method Courses|null find($id, $lockMode = null, $lockVersion = null)
 * @method Courses|null findOneBy(array $criteria, array $orderBy = null)
 * @method Courses[]    findAll()
 * @method Courses[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CoursesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Courses::class);
    }

    /**
     * @param $clientId
     * @return Courses[] Returns an array of Courses objects
     */
    public function findAllCoursesByClient($clientId){
        return $this->createQueryBuilder('c')
            ->select('c','st','cl','ta','tax','se')
            ->innerJoin('c.status', 'st')
            ->join('c.client', 'cl')
            ->join('c.tarif', 'ta')
            ->join('c.Taxis', 'tax')
            ->join('c.secteur', 'se')
            ->andWhere('cl.id = :val')
            ->setParameter('val', $clientId)
            ->orderBy('st.st_libelle', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param $taxisId
     * @return Courses[] Returns an array of Courses objects
     */
    public function findAllCoursesOfTaxisAcceptedOrTerminetated($taxisId){
        return $this->createQueryBuilder('c')
            ->select('c','st','cl','ta','tax','se')
            ->innerJoin('c.status', 'st')
            ->join('c.client', 'cl')
            ->join('c.tarif', 'ta')
            ->join('c.Taxis', 'tax')
            ->join('c.secteur', 'se')
            ->andWhere('tax.id = :val')
            ->setParameter('val', $taxisId)
            ->andWhere('st.st_libelle = \'accepte\'')
            ->orWhere('st.st_libelle = \'termine\'')
            ->orderBy('st.st_libelle', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param $taxisId
     * @return Courses[] Returns an array of Courses objects
     */
    public function findAllCoursesOfTaxisWaiting($taxisId){
        return $this->createQueryBuilder('c')
            ->select('c','st','cl','ta','tax','se')
            ->innerJoin('c.status', 'st')
            ->join('c.client', 'cl')
            ->join('c.tarif', 'ta')
            ->join('c.Taxis', 'tax')
            ->join('c.secteur', 'se')
            ->andWhere('tax.id = :val')
            ->setParameter('val', $taxisId)
            ->andWhere('st.st_libelle = \'attente\'')
            ->orderBy('st.st_libelle', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }
    // /**
    //  * @return Courses[] Returns an array of Courses objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Courses
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
