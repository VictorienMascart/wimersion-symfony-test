<?php

namespace App\Form;

use App\Entity\Courses;
use App\Entity\Secteurs;
use App\Entity\Tarifs;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CourseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('co_date',DateType::class)
            ->add('co_heure',TimeType::class)
            ->add('co_place_necessaire',NumberType::class)
            ->add('co_prise_charge',TextType::class)
            ->add('co_destination',TextType::class)
            ->add('secteur', EntityType::class, [
                'class' => Secteurs::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.se_libelle', 'ASC');
                },
                'choice_label' => 'se_libelle'])
            ->add('tarif', EntityType::class, [
                'class' => Tarifs::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.tf_prixttc_km', 'ASC');
                },
                'choice_label' => 'tf_prixttc_km'])
            ->add('annuler',ResetType::class, [
                'attr' => ['class' => 'annuler'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Courses::class,
        ]);
    }
}
