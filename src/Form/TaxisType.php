<?php

namespace App\Form;

use App\Entity\Secteurs;
use App\Entity\Tarifs;
use App\Entity\Taxis;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaxisType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ta_nom',TextType::class)
            ->add('ta_prenom',TextType::class)
            ->add('ta_portable',TelType::class)
            ->add('ta_mail',EmailType::class)
            ->add('plainPassword',RepeatedType::class,array(
                'type' => PasswordType::class,
                'first_options' => array('label'=>'Password'),
                'second_options' => array('label'=>'Repeat Password')))
            ->add('secteur', EntityType::class, [
                'class' => Secteurs::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.se_libelle', 'ASC');
                },
                'choice_label' => 'se_libelle',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Taxis::class,
        ]);
    }
}
