<?php

namespace App\Form;

use App\Entity\Clients;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientInscriptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cl_nom',TextType::class)
            ->add('cl_prenom', TextType::class)
            ->add('cl_portable',TelType::class)
            ->add('cl_mail',EmailType::class)
            ->add('PlainPassword',RepeatedType::class,array(
                'type' => PasswordType::class,
                'first_options' => array('label'=>'Password'),
                'second_options' => array('label'=>'Repeat Password')
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Clients::class,
        ]);
    }
}
