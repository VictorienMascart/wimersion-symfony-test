<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200218085356 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE courses ADD taxis_id INT DEFAULT NULL, ADD client_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE courses ADD CONSTRAINT FK_A9A55A4CF3208E92 FOREIGN KEY (taxis_id) REFERENCES taxis (id)');
        $this->addSql('ALTER TABLE courses ADD CONSTRAINT FK_A9A55A4C19EB6921 FOREIGN KEY (client_id) REFERENCES clients (id)');
        $this->addSql('CREATE INDEX IDX_A9A55A4CF3208E92 ON courses (taxis_id)');
        $this->addSql('CREATE INDEX IDX_A9A55A4C19EB6921 ON courses (client_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE courses DROP FOREIGN KEY FK_A9A55A4CF3208E92');
        $this->addSql('ALTER TABLE courses DROP FOREIGN KEY FK_A9A55A4C19EB6921');
        $this->addSql('DROP INDEX IDX_A9A55A4CF3208E92 ON courses');
        $this->addSql('DROP INDEX IDX_A9A55A4C19EB6921 ON courses');
        $this->addSql('ALTER TABLE courses DROP taxis_id, DROP client_id');
    }
}
