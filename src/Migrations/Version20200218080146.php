<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200218080146 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE avis MODIFY av_id INT NOT NULL');
        $this->addSql('ALTER TABLE avis DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE avis CHANGE av_id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE avis ADD CONSTRAINT FK_8F91ABF019EB6921 FOREIGN KEY (client_id) REFERENCES clients (id)');
        $this->addSql('ALTER TABLE avis ADD CONSTRAINT FK_8F91ABF0591CC992 FOREIGN KEY (course_id) REFERENCES courses (id)');
        $this->addSql('ALTER TABLE avis ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE courses ADD CONSTRAINT FK_A9A55A4C9F7E4405 FOREIGN KEY (secteur_id) REFERENCES secteurs (id)');
        $this->addSql('ALTER TABLE courses ADD CONSTRAINT FK_A9A55A4C357C0A59 FOREIGN KEY (tarif_id) REFERENCES tarifs (id)');
        $this->addSql('ALTER TABLE courses ADD CONSTRAINT FK_A9A55A4C6BF700BD FOREIGN KEY (status_id) REFERENCES status (id)');
        $this->addSql('ALTER TABLE taxis ADD CONSTRAINT FK_6E3BE8F4292FFF1D FOREIGN KEY (vehicule) REFERENCES vehicules (immat)');
        $this->addSql('ALTER TABLE taxis ADD CONSTRAINT FK_6E3BE8F49F7E4405 FOREIGN KEY (secteur_id) REFERENCES secteurs (id)');
        $this->addSql('ALTER TABLE taxis ADD CONSTRAINT FK_6E3BE8F4357C0A59 FOREIGN KEY (tarif_id) REFERENCES tarifs (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE avis MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE avis DROP FOREIGN KEY FK_8F91ABF019EB6921');
        $this->addSql('ALTER TABLE avis DROP FOREIGN KEY FK_8F91ABF0591CC992');
        $this->addSql('ALTER TABLE avis DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE avis CHANGE id av_id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE avis ADD PRIMARY KEY (av_id)');
        $this->addSql('ALTER TABLE courses DROP FOREIGN KEY FK_A9A55A4C9F7E4405');
        $this->addSql('ALTER TABLE courses DROP FOREIGN KEY FK_A9A55A4C357C0A59');
        $this->addSql('ALTER TABLE courses DROP FOREIGN KEY FK_A9A55A4C6BF700BD');
        $this->addSql('ALTER TABLE taxis DROP FOREIGN KEY FK_6E3BE8F4292FFF1D');
        $this->addSql('ALTER TABLE taxis DROP FOREIGN KEY FK_6E3BE8F49F7E4405');
        $this->addSql('ALTER TABLE taxis DROP FOREIGN KEY FK_6E3BE8F4357C0A59');
    }
}
