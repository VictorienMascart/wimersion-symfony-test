<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200219192229 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE avis (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, course_id INT DEFAULT NULL, cl_note DOUBLE PRECISION NOT NULL, cl_avis VARCHAR(255) NOT NULL, INDEX IDX_8F91ABF019EB6921 (client_id), UNIQUE INDEX UNIQ_8F91ABF0591CC992 (course_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE clients (id INT AUTO_INCREMENT NOT NULL, cl_nom VARCHAR(50) NOT NULL, cl_prenom VARCHAR(50) NOT NULL, cl_portable VARCHAR(10) NOT NULL, cl_mail VARCHAR(100) NOT NULL, cl_mdp VARCHAR(200) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE courses (id INT AUTO_INCREMENT NOT NULL, secteur_id INT DEFAULT NULL, tarif_id INT DEFAULT NULL, status_id INT DEFAULT NULL, taxis_id INT DEFAULT NULL, client_id INT DEFAULT NULL, co_date DATE NOT NULL, co_heure DATETIME NOT NULL, co_place_necessaire INT NOT NULL, co_prise_charge VARCHAR(50) NOT NULL, co_destination VARCHAR(50) NOT NULL, INDEX IDX_A9A55A4C9F7E4405 (secteur_id), INDEX IDX_A9A55A4C357C0A59 (tarif_id), INDEX IDX_A9A55A4C6BF700BD (status_id), INDEX IDX_A9A55A4CF3208E92 (taxis_id), INDEX IDX_A9A55A4C19EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE secteurs (id INT AUTO_INCREMENT NOT NULL, se_libelle VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE status (id INT AUTO_INCREMENT NOT NULL, st_libelle VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tarifs (id INT AUTO_INCREMENT NOT NULL, tf_libelle VARCHAR(50) NOT NULL, tf_prixttc_km INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE taxis (id INT AUTO_INCREMENT NOT NULL, vehicule VARCHAR(9) DEFAULT NULL, secteur_id INT DEFAULT NULL, tarif_id INT DEFAULT NULL, ta_nom VARCHAR(50) NOT NULL, ta_prenom VARCHAR(50) NOT NULL, ta_portable VARCHAR(10) NOT NULL, ta_mail VARCHAR(100) NOT NULL, ta_mdp VARCHAR(100) NOT NULL, UNIQUE INDEX UNIQ_6E3BE8F4292FFF1D (vehicule), INDEX IDX_6E3BE8F49F7E4405 (secteur_id), INDEX IDX_6E3BE8F4357C0A59 (tarif_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vehicules (immat VARCHAR(9) NOT NULL, ve_marque VARCHAR(255) NOT NULL, ve_model VARCHAR(255) NOT NULL, ve_nbr_place INT NOT NULL, ve_energie VARCHAR(10) NOT NULL, PRIMARY KEY(immat)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE avis ADD CONSTRAINT FK_8F91ABF019EB6921 FOREIGN KEY (client_id) REFERENCES clients (id)');
        $this->addSql('ALTER TABLE avis ADD CONSTRAINT FK_8F91ABF0591CC992 FOREIGN KEY (course_id) REFERENCES courses (id)');
        $this->addSql('ALTER TABLE courses ADD CONSTRAINT FK_A9A55A4C9F7E4405 FOREIGN KEY (secteur_id) REFERENCES secteurs (id)');
        $this->addSql('ALTER TABLE courses ADD CONSTRAINT FK_A9A55A4C357C0A59 FOREIGN KEY (tarif_id) REFERENCES tarifs (id)');
        $this->addSql('ALTER TABLE courses ADD CONSTRAINT FK_A9A55A4C6BF700BD FOREIGN KEY (status_id) REFERENCES status (id)');
        $this->addSql('ALTER TABLE courses ADD CONSTRAINT FK_A9A55A4CF3208E92 FOREIGN KEY (taxis_id) REFERENCES taxis (id)');
        $this->addSql('ALTER TABLE courses ADD CONSTRAINT FK_A9A55A4C19EB6921 FOREIGN KEY (client_id) REFERENCES clients (id)');
        $this->addSql('ALTER TABLE taxis ADD CONSTRAINT FK_6E3BE8F4292FFF1D FOREIGN KEY (vehicule) REFERENCES vehicules (immat)');
        $this->addSql('ALTER TABLE taxis ADD CONSTRAINT FK_6E3BE8F49F7E4405 FOREIGN KEY (secteur_id) REFERENCES secteurs (id)');
        $this->addSql('ALTER TABLE taxis ADD CONSTRAINT FK_6E3BE8F4357C0A59 FOREIGN KEY (tarif_id) REFERENCES tarifs (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE avis DROP FOREIGN KEY FK_8F91ABF019EB6921');
        $this->addSql('ALTER TABLE courses DROP FOREIGN KEY FK_A9A55A4C19EB6921');
        $this->addSql('ALTER TABLE avis DROP FOREIGN KEY FK_8F91ABF0591CC992');
        $this->addSql('ALTER TABLE courses DROP FOREIGN KEY FK_A9A55A4C9F7E4405');
        $this->addSql('ALTER TABLE taxis DROP FOREIGN KEY FK_6E3BE8F49F7E4405');
        $this->addSql('ALTER TABLE courses DROP FOREIGN KEY FK_A9A55A4C6BF700BD');
        $this->addSql('ALTER TABLE courses DROP FOREIGN KEY FK_A9A55A4C357C0A59');
        $this->addSql('ALTER TABLE taxis DROP FOREIGN KEY FK_6E3BE8F4357C0A59');
        $this->addSql('ALTER TABLE courses DROP FOREIGN KEY FK_A9A55A4CF3208E92');
        $this->addSql('ALTER TABLE taxis DROP FOREIGN KEY FK_6E3BE8F4292FFF1D');
        $this->addSql('DROP TABLE avis');
        $this->addSql('DROP TABLE clients');
        $this->addSql('DROP TABLE courses');
        $this->addSql('DROP TABLE secteurs');
        $this->addSql('DROP TABLE status');
        $this->addSql('DROP TABLE tarifs');
        $this->addSql('DROP TABLE taxis');
        $this->addSql('DROP TABLE vehicules');
    }
}
