<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientsRepository")
 */
class Clients implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $cl_nom;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $cl_prenom;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cl_portable;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $cl_mail;

    /**
     * @Assert\NotBlank()
     */
    private $plainPassword;
    /**
     * @ORM\Column(type="string", length=200)
     */
    private $cl_mdp;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Avis", mappedBy="client")
     *
     */
    private $avis;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Courses", mappedBy="client")
     */
    private $courses;

    public function __construct()
    {
        $this->avis = new ArrayCollection();
        $this->courses = new ArrayCollection();
    }

    public function getClId(): ?int
    {
        return $this->id;
    }

    public function getClNom(): ?string
    {
        return $this->cl_nom;
    }

    public function setClNom(string $cl_nom): self
    {
        $this->cl_nom = $cl_nom;

        return $this;
    }

    public function getClPrenom(): ?string
    {
        return $this->cl_prenom;
    }

    public function setClPrenom(string $cl_prenom): self
    {
        $this->cl_prenom = $cl_prenom;

        return $this;
    }

    public function getClPortable(): ?string
    {
        return $this->cl_portable;
    }

    public function setClPortable(string $cl_portable): self
    {
        $this->cl_portable = $cl_portable;

        return $this;
    }

    public function getClMail(): ?string
    {
        return $this->cl_mail;
    }

    public function setClMail(string $cl_mail): self
    {
        $this->cl_mail = $cl_mail;

        return $this;
    }

    public function getPlainPassword(){
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    public function getClMdp(): ?string
    {
        return $this->cl_mdp;
    }

    public function setClMdp(string $cl_mdp): self
    {
        $this->cl_mdp = $cl_mdp;

        return $this;
    }

    /**
     * @return Collection|Avis[]
     */
    public function getAvis(): Collection
    {
        return $this->avis;
    }

    public function addAvi(Avis $avi): self
    {
        if (!$this->avis->contains($avi)) {
            $this->avis[] = $avi;
            $avi->setClient($this);
        }

        return $this;
    }

    public function removeAvi(Avis $avi): self
    {
        if ($this->avis->contains($avi)) {
            $this->avis->removeElement($avi);
            // set the owning side to null (unless already changed)
            if ($avi->getClient() === $this) {
                $avi->setClient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Courses[]
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(Courses $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
            $course->setClient($this);
        }

        return $this;
    }

    public function removeCourse(Courses $course): self
    {
        if ($this->courses->contains($course)) {
            $this->courses->removeElement($course);
            // set the owning side to null (unless already changed)
            if ($course->getClient() === $this) {
                $course->setClient(null);
            }
        }

        return $this;
    }


    /**
     * Returns the roles granted to the user.
     *
     *     public function getRoles()
     *     {
     *         return ['ROLE_USER'];
     *     }
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return string[] The user roles
     */
    public function getRoles()
    {
        return array('ROLE_CLIENT');
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string|null The encoded password if any
     */
    public function getPassword()
    {
        return $this->cl_mdp;
    }

    public function setPassword($pass)
    {
        return $this->cl_mdp = $pass;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
       return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->cl_mail;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}
