<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AvisRepository")
 */
class Avis
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $av_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Clients", inversedBy="avis")
     *
     */
    private $client;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Courses", cascade={"persist", "remove"})
     */
    private $course;

    /**
     * @ORM\Column(type="float")
     */
    private $cl_note;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cl_avis;

    public function getAvId(): ?int
    {
        return $this->av_id;
    }

    public function getClient(): ?Clients
    {
        return $this->client;
    }

    public function setClient(?Clients $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getCourse(): ?Courses
    {
        return $this->course;
    }

    public function setCourse(?Courses $course): self
    {
        $this->course = $course;

        return $this;
    }

    public function getClNote(): ?float
    {
        return $this->cl_note;
    }

    public function setClNote(float $cl_note): self
    {
        $this->cl_note = $cl_note;

        return $this;
    }

    public function getClAvis(): ?string
    {
        return $this->cl_avis;
    }

    public function setClAvis(string $cl_avis): self
    {
        $this->cl_avis = $cl_avis;

        return $this;
    }
}
