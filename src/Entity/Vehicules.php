<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VehiculesRepository")
 */
class Vehicules
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=9)
     */
    private $immat;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ve_marque;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ve_model;

    /**
     * @ORM\Column(type="integer")
     */
    private $ve_nbr_place;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $ve_energie;

    public function getImmat(): ?string
    {
        return $this->immat;
    }

    public function setImmat(string $immat): self
    {
        $this->immat = $immat;

        return $this;
    }

    public function getVeMarque(): ?string
    {
        return $this->ve_marque;
    }

    public function setVeMarque(string $ve_marque): self
    {
        $this->ve_marque = $ve_marque;

        return $this;
    }

    public function getVeModel(): ?string
    {
        return $this->ve_model;
    }

    public function setVeModel(string $ve_model): self
    {
        $this->ve_model = $ve_model;

        return $this;
    }

    public function getVeNbrPlace(): ?int
    {
        return $this->ve_nbr_place;
    }

    public function setVeNbrPlace(int $ve_nbr_place): self
    {
        $this->ve_nbr_place = $ve_nbr_place;

        return $this;
    }

    public function getVeEnergie(): ?string
    {
        return $this->ve_energie;
    }

    public function setVeEnergie(string $ve_energie): self
    {
        $this->ve_energie = $ve_energie;

        return $this;
    }
}
