<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CoursesRepository")
 */
class Courses
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $co_date;

    /**
     * @ORM\Column(type="time")
     */
    private $co_heure;

    /**
     * @ORM\Column(type="integer")
     */
    private $co_place_necessaire;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $co_prise_charge;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $co_destination;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Secteurs", inversedBy="courses")
     */
    private $secteur;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tarifs", inversedBy="courses")
     */
    private $tarif;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Status", inversedBy="courses")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Taxis", inversedBy="courses")
     */
    private $Taxis;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Clients", inversedBy="courses")
     */
    private $client;


    public function getCoId(): ?int
    {
        return $this->id;
    }

    public function getCoDate(): ?\DateTimeInterface
    {
        return $this->co_date;
    }

    public function setCoDate(\DateTimeInterface $co_date): self
    {
        $this->co_date = $co_date;

        return $this;
    }

    public function getCoHeure(): ?\DateTimeInterface
    {
        return $this->co_heure;
    }

    public function setCoHeure(\DateTimeInterface $co_heure): self
    {
        $this->co_heure = $co_heure;

        return $this;
    }

    public function getCoPlaceNecessaire(): ?int
    {
        return $this->co_place_necessaire;
    }

    public function setCoPlaceNecessaire(int $co_place_necessaire): self
    {
        $this->co_place_necessaire = $co_place_necessaire;

        return $this;
    }

    public function getCoPriseCharge(): ?string
    {
        return $this->co_prise_charge;
    }

    public function setCoPriseCharge(string $co_prise_charge): self
    {
        $this->co_prise_charge = $co_prise_charge;

        return $this;
    }

    public function getCoDestination(): ?string
    {
        return $this->co_destination;
    }

    public function setCoDestination(string $co_destination): self
    {
        $this->co_destination = $co_destination;

        return $this;
    }

    public function getSecteur(): ?Secteurs
    {
        return $this->secteur;
    }

    public function setSecteur(?Secteurs $secteur): self
    {
        $this->secteur = $secteur;

        return $this;
    }

    public function getTarif(): ?Tarifs
    {
        return $this->tarif;
    }

    public function setTarif(?Tarifs $tarif): self
    {
        $this->tarif = $tarif;

        return $this;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(?Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getTaxis(): ?Taxis
    {
        return $this->Taxis;
    }

    public function setTaxis(?Taxis $Taxis): self
    {
        $this->Taxis = $Taxis;

        return $this;
    }

    public function getClient(): ?Clients
    {
        return $this->client;
    }

    public function setClient(?Clients $client): self
    {
        $this->client = $client;

        return $this;
    }
}
