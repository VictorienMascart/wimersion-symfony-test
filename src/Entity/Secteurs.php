<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SecteursRepository")
 */
class Secteurs
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $se_libelle;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Courses", mappedBy="secteur")
     */
    private $courses;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Taxis", mappedBy="secteur")
     */
    private $taxes;

    public function __construct()
    {
        $this->courses = new ArrayCollection();
        $this->taxes = new ArrayCollection();
    }

    public function getSeId(): ?int
    {
        return $this->id;
    }

    public function getSeLibelle(): ?string
    {
        return $this->se_libelle;
    }

    public function setSeLibelle(string $se_libelle): self
    {
        $this->se_libelle = $se_libelle;

        return $this;
    }

    /**
     * @return Collection|Courses[]
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(Courses $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
            $course->setSecteur($this);
        }

        return $this;
    }

    public function removeCourse(Courses $course): self
    {
        if ($this->courses->contains($course)) {
            $this->courses->removeElement($course);
            // set the owning side to null (unless already changed)
            if ($course->getSecteur() === $this) {
                $course->setSecteur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Taxis[]
     */
    public function getTaxes(): Collection
    {
        return $this->taxes;
    }

    public function addTax(Taxis $tax): self
    {
        if (!$this->taxes->contains($tax)) {
            $this->taxes[] = $tax;
            $tax->setSecteur($this);
        }

        return $this;
    }

    public function removeTax(Taxis $tax): self
    {
        if ($this->taxes->contains($tax)) {
            $this->taxes->removeElement($tax);
            // set the owning side to null (unless already changed)
            if ($tax->getSecteur() === $this) {
                $tax->setSecteur(null);
            }
        }

        return $this;
    }
}
