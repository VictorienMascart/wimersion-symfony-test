<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TaxisRepository")
 */
class Taxis implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $ta_nom;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $ta_prenom;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $ta_portable;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $ta_mail;

    /**
     * @Assert\NotBlank()
     */
    private $plainPassword;
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $ta_mdp;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Vehicules", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="vehicule", referencedColumnName="immat")
     */
    private $vehicule;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Secteurs", inversedBy="taxes")
     */
    private $secteur;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tarifs", inversedBy="taxes",cascade={"persist"})
     */
    private $tarif;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Courses", mappedBy="Taxis")
     */
    private $courses;

    public function __construct()
    {
        $this->courses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTaNom(): ?string
    {
        return $this->ta_nom;
    }

    public function setTaNom(string $tanom): self
    {
        $this->ta_nom = $tanom;

        return $this;
    }

    public function getTaPrenom(): ?string
    {
        return $this->ta_prenom;
    }

    public function setTaPrenom(string $taprenom): self
    {
        $this->ta_prenom = $taprenom;

        return $this;
    }

    public function getTaPortable(): ?string
    {
        return $this->ta_portable;
    }

    public function setTaPortable(string $taportable): self
    {
        $this->ta_portable = $taportable;

        return $this;
    }

    public function getTaMail(): ?string
    {
        return $this->ta_mail;
    }

    public function setTaMail(string $tamail): self
    {
        $this->ta_mail = $tamail;

        return $this;
    }

    public function getPlainPassword(){
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    public function getTaMdp(): ?string
    {
        return $this->ta_mdp;
    }

    public function setTaMdp(string $tamdp): self
    {
        $this->ta_mdp = $tamdp;

        return $this;
    }

    public function getVehicule(): ?Vehicules
    {
        return $this->vehicule;
    }

    public function setVehicule(?Vehicules $vehicule): self
    {
        $this->vehicule = $vehicule;

        return $this;
    }

    public function getSecteur(): ?Secteurs
    {
        return $this->secteur;
    }

    public function setSecteur(?Secteurs $secteur): self
    {
        $this->secteur = $secteur;

        return $this;
    }

    public function getTarif(): ?Tarifs
    {
        return $this->tarif;
    }

    public function setTarif(?Tarifs $tarif): self
    {
        $this->tarif = $tarif;

        return $this;
    }

    /**
     * @return Collection|Courses[]
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(Courses $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
            $course->setTaxis($this);
        }

        return $this;
    }

    public function removeCourse(Courses $course): self
    {
        if ($this->courses->contains($course)) {
            $this->courses->removeElement($course);
            // set the owning side to null (unless already changed)
            if ($course->getTaxis() === $this) {
                $course->setTaxis(null);
            }
        }

        return $this;
    }

    /**
     * Returns the roles granted to the user.
     *
     *     public function getRoles()
     *     {
     *         return ['ROLE_USER'];
     *     }
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return string[] The user roles
     */
    public function getRoles()
    {
        return array('ROLE_CHAUFFEUR');
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string|null The encoded password if any
     */
    public function getPassword()
    {
        return $this->ta_mdp;
    }
    public function setPassword($pass)
    {
        return $this->ta_mdp = $pass;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->ta_mail;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}
