<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TarifsRepository")
 */
class Tarifs
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $tf_libelle;

    /**
     * @ORM\Column(type="integer")
     */
    private $tf_prixttc_km;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Courses", mappedBy="tarif")
     */
    private $courses;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Taxis", mappedBy="tarif")
     */
    private $taxes;

    public function __construct()
    {
        $this->courses = new ArrayCollection();
        $this->taxes = new ArrayCollection();
    }

    public function getTaId(): ?int
    {
        return $this->id;
    }

    public function getTfLibelle(): ?string
    {
        return $this->tf_libelle;
    }

    public function setTfLibelle(string $tf_libelle): self
    {
        $this->tf_libelle = $tf_libelle;

        return $this;
    }

    public function getTfPrixttcKm(): ?int
    {
        return $this->tf_prixttc_km;
    }

    public function setTfPrixttcKm(int $tf_prixttc_km): self
    {
        $this->tf_prixttc_km = $tf_prixttc_km;

        return $this;
    }

    /**
     * @return Collection|Courses[]
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(Courses $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
            $course->setTarif($this);
        }

        return $this;
    }

    public function removeCourse(Courses $course): self
    {
        if ($this->courses->contains($course)) {
            $this->courses->removeElement($course);
            // set the owning side to null (unless already changed)
            if ($course->getTarif() === $this) {
                $course->setTarif(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Taxis[]
     */
    public function getTaxes(): Collection
    {
        return $this->taxes;
    }

    public function addTax(Taxis $tax): self
    {
        if (!$this->taxes->contains($tax)) {
            $this->taxes[] = $tax;
            $tax->setTarif($this);
        }

        return $this;
    }

    public function removeTax(Taxis $tax): self
    {
        if ($this->taxes->contains($tax)) {
            $this->taxes->removeElement($tax);
            // set the owning side to null (unless already changed)
            if ($tax->getTarif() === $this) {
                $tax->setTarif(null);
            }
        }

        return $this;
    }
}
