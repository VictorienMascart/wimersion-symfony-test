<?php
namespace App\Controller;
use App\Entity\Clients;
use App\Entity\Courses;
use App\Entity\Status;
use App\Entity\Taxis;
use App\Form\ClientInterfaceType;
use App\Form\TaxisType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
class TaxisController extends AbstractController
{
    /**
     * @Route("/taxis", name="taxis")
     */
    public function index()
    {
        return $this->render('taxis/index.html.twig', [
            'controller_name' => 'TaxisController',
        ]);
    }
    /**
     * @Route("/chauffeur/{id}", name="ShowInterfaceChauffeur", methods={"GET","POST"})
     * @param Request $request
     * @param Taxis $taxis
     * @return Response
     */
    public function show(Request $request ,Taxis $taxis) : Response
    {
//        $form = $this->createForm(TaxisType::class,$taxis);
//        $Manager = $this->getDoctrine()->getManager();
//        $tabCoursesOfTaxisAcceptedOrTerminetated = $Manager->getRepository(Courses::class)->findAllCoursesOfTaxisAcceptedOrTerminetated($taxis->getId());
//        $tabCoursesOfTaxisWaiting = $Manager->getRepository(Courses::class)->findAllCoursesOfTaxisWaiting($taxis->getId());
        return $this->render('taxis/index.html.twig', [
        ]);
    }
}
