<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SecteursController extends AbstractController
{
    /**
     * @Route("/secteurs", name="secteurs")
     */
    public function index()
    {
        return $this->render('secteurs/index.html.twig', [
            'controller_name' => 'SecteursController',
        ]);
    }
}
