<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class VehiculesController extends AbstractController
{
    /**
     * @Route("/vehicules", name="vehicules")
     */
    public function index()
    {
        return $this->render('vehicules/index.html.twig', [
            'controller_name' => 'VehiculesController',
        ]);
    }
}
