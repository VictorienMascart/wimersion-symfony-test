<?php

namespace App\Controller;

use App\Entity\Secteurs;
use App\Entity\Tarifs;
use App\Entity\Taxis;
use App\Entity\Vehicules;
use App\Entity\Clients;
use App\Form\ClientInscriptionType;
use App\Form\ChauffeurInscriptionType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class InscriptionController extends AbstractController
{
    /**
     * @Route("/inscription", name="inscriptionAccueil")
     */
    public function index()
    {
        return $this->render('inscription/index.html.twig', [
            'controller_name' => 'InscriptionController'
        ]);
    }

    /**
     * @Route("/inscription/Client", name="inscriptionClient")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function registrationClient(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $client = new Clients();
        $form = $this->createForm(ClientInscriptionType::class, $client);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($client, $client->getPlainPassword());
            $client->setClMdp($password);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($client);
            $entityManager->flush();
            return $this->render('security/index.html.twig');
        }
        return $this->render('inscription/clientinscription.html.twig', array('form'=>$form->createView()));
    }

    /**
     * @Route("/inscription/Chauffeur", name="inscriptionChauffeur")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function registrationChauffeur(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $taxis = new Taxis();
        $vehicule = new Vehicules();
        $tarif = new Tarifs();
        $form = $this->createForm(ChauffeurInscriptionType::class, ['taxis'=>$taxis,'vehicule'=>$vehicule,'tarif'=>$tarif]);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($taxis, $taxis->getPlainPassword());
            $taxis->setTaMdp($password);

            $taxis->setVehicule($vehicule);
            $taxis->setTarif($tarif);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($taxis);
            $entityManager->flush();
            return $this->render('security/index.html.twig');
        }


        return $this->render('inscription/taxisinscription.html.twig', array('form'=>$form->createView()));
    }
}
