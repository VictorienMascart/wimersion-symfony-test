<?php

namespace App\Controller;

use App\Entity\Clients;
use App\Entity\Courses;
use App\Entity\Status;
use App\Entity\Taxis;
use App\Form\ClientInscriptionType;
use App\Form\ClientInterfaceType;
use App\Form\CourseType;
use Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ClientsController extends AbstractController
{
    /**
     * @Route("/client", name="client")
     */
    public function index()
    {
        return $this->render('client/index.html.twig', [
            'controller_name' => 'ClientsController',
        ]);
    }

    /**
     * @Route("/client/{id}", name="ShowInterfaceClient", methods={"GET","POST"})
     * @param Request $request
     * @param Clients $client
     * @return Response
     */
    public function show(Request $request ,Clients $client) : Response
    {
        $course = new Courses();
        $form = $this->createForm(ClientInterfaceType::class, ['client'=>$client,'course'=>$course]);
        $Manager = $this->getDoctrine()->getManager();
        $tab = $Manager->getRepository(Courses::class)->findAllCoursesByClient($client->getClId());

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $course->setClient($client);
            $status = $Manager->getRepository(Status::class)->findOneBy(array("st_libelle"=>"attente"));
            $course->setStatus($status);
            $Manager->persist($course);
            $Manager->persist($client);
            $Manager->flush();

            return $this->redirectToRoute('ChooseTaxis',array('course'=>$course->getCoId()));


        }

        return $this->render('client/interfaceclient.html.twig', [
            'form'=>$form->createView(),
            "listCourses"=>$tab
        ]);
    }



    /**
     * @Route("/client/listTaxis/{course}", name="ChooseTaxis", methods={"GET","POST"})
     * @param $request
     * @param $course
     * @return Response
     */
    public function chooseTaxis(Request $request ,Courses $course) : Response
    {

        $Manager = $this->getDoctrine()->getManager();
        $tab = $Manager->getRepository(Taxis::class)->findAllTaxisForParam($course);
        return $this->render('client/interfacechooseTaxis.html.twig', [
            'listTaxis' => $tab,
            'course'=>$course
        ]);
    }

    /**
     * @Route("/client/listTaxis/{course}/{taxis}", name="TaxisChoosed", methods={"GET","POST"})
     * @param Courses $course
     * @param Taxis $taxis
     * @return Response
     */
    public function Taxischoosed(Request $request, Courses $course,Taxis $taxis) : Response
    {

        $Manager = $this->getDoctrine()->getManager();
//        $course = $Manager->getRepository(Courses::class)->findOneBy(array("id"=>$course->getCoId()));
//        $taxis = $Manager->getRepository(Taxis::class)->findOneBy(array("id"=>$taxis->getId()));

        $course->setTaxis($taxis);
        $Manager->persist($course);
        $Manager->flush();

        return $this->redirectToRoute('ShowInterfaceClient',array('id'=>$course->getClient()->getClId()));

    }


}
