<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ConnexionController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     */
    public function index()
    {
        return $this->render('security/index.html.twig');
    }
    /**
     * @Route("", name="logout")
     */
    public function logout()
    {
        return $this->render('security/index.html.twig');
    }
}
